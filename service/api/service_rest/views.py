from django.shortcuts import render
from django.http import JsonResponse
from .models import Technician, Appointment, AutomobileVO
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder, DateEncoder
from .encoders import AppointmentDetailEncoder, AppointmentListEncoder, TechnicianListEncoder, TechnicianDetailEncoder, AutomobileVOEncoder
from django.forms.models import model_to_dict


from django.http import JsonResponse

@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        appointment_dicts = [model_to_dict(appointment) for appointment in appointments]
        return JsonResponse(appointment_dicts, safe=False)
    
    else: # request.method == "POST":
        content = json.loads(request.body)
        
        if "vin" in content:
            vin = content["vin"]
            try:
                automobile_vo = AutomobileVO.objects.get(vin=vin)
                automobile = automobile_vo.automobile
                content["vip"] = automobile.sold
            except AutomobileVO.DoesNotExist:
                content["vip"] = False

        if "technician" in content:
            technician_eid = content["technician"]["employee_id"]
            try:
                technician = Technician.objects.get(employee_id=technician_eid)
                content["technician"] = technician
            except Technician.DoesNotExist:
                return JsonResponse({"error": "Technician not found"})
        
        appointment = Appointment.objects.create(**content)

        return JsonResponse(
            model_to_dict(appointment),
            safe=False,
        )
    

@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_appointment(request, id):
    if request.method == "GET":
        appointment = Appointment.objects.get(id=id)
        appointment_dict = model_to_dict(appointment)

        return JsonResponse(appointment_dict, safe=False)
        
    elif request.method == "DELETE":
        count, _ = Appointment.objects.filter(id=id).delete()

        return JsonResponse({"deleted": count > 0})

    elif request.method == "PUT":
        appointment = Appointment.objects.get(id=id)
        content = json.loads(request.body)
        
        for key, value in content.items():
            setattr(appointment, key, value)
            
        appointment.save()

        return JsonResponse(appointment, encoder=AppointmentDetailEncoder, safe=False)

@require_http_methods(["PUT"])
def api_change_status_appointment(request, id):
    if request.method == "PUT":
        try:
            appointment = Appointment.objects.get(id=id)
            content = json.loads(request.body)
            status = content.get("status", "")

            if status.lower() == "cancel":
                appointment.status = "Canceled"
            
            elif status.lower() == "finish":
                appointment.status = "Finished"
            
            else:
                return JsonResponse(
                    {"error": "Valid status options are 'Active', 'Cancel', or 'Finish'"},
                    status=400,
                )
            appointment.save()

            response_data = {
                "message": "Appointment status successfully updated!",
                "status": appointment.status,
            }

            return JsonResponse(response_data, safe=False)
        except Appointment.DoesNotExist:

            return JsonResponse(
                {"error": "Appointment not found"},
                status=404,
            )
        
    



@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        
        return JsonResponse({"technicians": technicians}, encoder=TechnicianListEncoder)

    else: # request.method == "POST":
        content = json.loads(request.body)
        technician = Technician.objects.create(**content)

        return JsonResponse(
            technician,
            encoder=TechnicianDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET"])
def api_show_technicians(request, employee_id):
    try:
        technician = Technician.objects.get(employee_id=employee_id)
    except Technician.DoesNotExist:
        return JsonResponse({"message": "Technician Not Found"}, status=404)

    if request.method == "GET":
        return JsonResponse(
            {"technician": technician},
            encoder=TechnicianDetailEncoder,
        )
    else: # request.method == "DELETE":
        count, _ = Technician.objects.filter(employee_id=technician.employee_id).delete()
        return JsonResponse({"deleted": count > 0})
    



@require_http_methods(["GET"])
def api_list_automobileVOs(request):
    autos = AutomobileVO.objects.all()
    
    return JsonResponse({"autos": autos}, encoder=AutomobileVOEncoder)