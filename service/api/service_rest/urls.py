from django.urls import path
from .views import (
    api_list_technicians,
    api_show_technicians,
    api_list_appointments,
    api_show_appointment,
    api_change_status_appointment,
    api_list_automobileVOs,
)

urlpatterns = [
    path("technicians/", api_list_technicians, name="api_list_technicians"),
    path("technicians/<str:employee_id>/", api_show_technicians, name="api_show_technicians"),
    path("technicians/<int:id>/", api_show_technicians, name="api_show_technicians"),
    path("appointments/", api_list_appointments, name="api_create_appointment"),
    path("appointments/<int:id>/", api_show_appointment, name="api_show_appointment"),
    path('appointments/<int:id>/finish/', api_change_status_appointment, name='api_finish_appointment'),
    path('appointments/<int:id>/cancel/', api_change_status_appointment, name='api_cancel_appointment'),
    path("automobileVOs/", api_list_automobileVOs, name="api_list_automobileVOs"),
]