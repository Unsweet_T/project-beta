from django.db import models
from django.urls import reverse
#import uuid


class Technician(models.Model):
    first_name = models.CharField(max_length=25)
    last_name = models.CharField(max_length=25)
    employee_id = models.CharField(max_length=25, unique=True)

    def __str__(self):
        return f"{self.employee_id}  {self.first_name} {self.last_name}"

    class Meta:
        app_label = "service_rest"
    
    
class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)
    import_href = models.CharField(max_length=250, unique=True)

    class Meta:
        app_label = "service_rest"


class Appointment(models.Model):
    status_field = [
        ("Active", "Active"),
        ("Canceled", "Canceled"),
        ("Finished", "Finished"),
    ]

    date_time = models.DateTimeField()
    customer = models.CharField(max_length=50, null=True)
    vip = models.BooleanField(default=False)
    customerreason = models.CharField(max_length=250, null=True)
    status = models.CharField(max_length=20, choices=status_field, default="Active")
    vin = models.CharField(max_length=17)
    technician_notes = models.TextField(max_length=500, null=True, blank=True)

    technician = models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
    )

    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="appointments",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
    )

    def __str__(self):
        return f"Customer: {self.customer}, Status: {self.status}"
    
    def get_api_url(self):
        return reverse("api_service", kwargs={"vin": self.id})

    class Meta:
        app_label = "service_rest"