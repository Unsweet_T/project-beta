from json import JSONEncoder
from django.urls import NoReverseMatch
from django.db.models import QuerySet
from datetime import datetime, date
from common.json import DateEncoder, QuerySetEncoder, ModelEncoder
from .models import AutomobileVO, Technician, Appointment


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold",
        "import_href", 
    ]


class TechnicianDetailEncoder(ModelEncoder):
    model = Technician
    properties = [
        "employee_id",
        "first_name",
        "last_name",
        "id",
    ]


class TechnicianListEncoder(ModelEncoder):
    model = Technician
    properties = [
        "id",
        "first_name",
        "last_name",
        "employee_id",
    ]


class AppointmentListEncoder(ModelEncoder, DateEncoder):
    model = Appointment
    properties = [
        "date_time",
        "customer",
        "customerreason",
        "status",
        "vin",
        "technician",
        "technician_notes",
        "vip",
    ]
    encoders = {
        "technician": TechnicianListEncoder(),
    }


class AppointmentDetailEncoder(ModelEncoder, DateEncoder):
    model = Appointment
    properties = [
        "date_time",
        "customer",
        "customerreason",
        "status",
        "vin",
        "technician",
        "technician_notes",
        "vip",
    ]
    encoders = {
        "technician": TechnicianListEncoder(),
    }