import { NavLink } from "react-router-dom";

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand fs-5" to="/">
          <span style={{ fontSize: "150%" }}>CarCar</span>
        </NavLink>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <div className="row">
            <div className="col-md-2">
              <NavLink
                className="navbar-brand small-link"
                to="/manufacturers"
                style={{ fontSize: "66%" }}
              >
                Manufacturers
              </NavLink>
            </div>
            <div className="col-md-2">
              <NavLink
                className="navbar-brand small-link"
                to="/new-manufacturer"
                style={{ fontSize: "66%" }}
              >
                New Manufacturer
              </NavLink>
            </div>
            <div className="col-md-2">
              <NavLink className="navbar-brand small-link" to="/models" style={{ fontSize: "66%" }}>
                Models
              </NavLink>
            </div>
            <div className="col-md-2">
              <NavLink className="navbar-brand small-link" to="/create-model" style={{ fontSize: "66%" }}>
                Create a Model
              </NavLink>
            </div>
            <div className="col-md-2">
              <NavLink className="navbar-brand small-link" to="/automobiles" style={{ fontSize: "66%" }}>
                Automobiles
              </NavLink>
            </div>
            <div className="col-md-2">
              <NavLink
                className="navbar-brand small-link"
                to="/create-automobile"
                style={{ fontSize: "66%" }}
              >
                Create an Automobile
              </NavLink>
            </div>
          </div>
          <div className="row">
            <div className="col-md-2">
              <NavLink className="navbar-brand small-link" to="/sales-team" style={{ fontSize: "66%" }}>
                Sales Team
              </NavLink>
            </div>
            <div className="col-md-2">
              <NavLink
                className="navbar-brand small-link"
                to="/add-sales-person"
                style={{ fontSize: "66%" }}
              >
                Add Sales Person
              </NavLink>
            </div>
            <div className="col-md-2">
              <NavLink className="navbar-brand small-link" to="/customer-list" style={{ fontSize: "66%" }}>
                Customer List
              </NavLink>
            </div>
            <div className="col-md-2">
              <NavLink className="navbar-brand small-link" to="/new-customer" style={{ fontSize: "66%" }}>
                New Customer
              </NavLink>
            </div>
            <div className="col-md-2">
              <NavLink className="navbar-brand small-link" to="/sales-list" style={{ fontSize: "66%" }}>
                Sales List
              </NavLink>
            </div>
            <div className="col-md-2">
              <NavLink className="navbar-brand small-link" to="/make-sale" style={{ fontSize: "66%" }}>
                Make a Sale
              </NavLink>
            </div>

          </div>
          <div className="row">
            <div className="col-md-2">
              <NavLink className="navbar-brand small-link" to="/technicians" style={{ fontSize: "66%" }}>
                Technicians
              </NavLink>
            </div>
            <div className="col-md-2">
              <NavLink className="navbar-brand small-link" to="/add-technician" style={{ fontSize: "66%" }}>
                Add a Technician
              </NavLink>
            </div>
            <div className="col-md-2">
              <NavLink className="navbar-brand small-link" to="/service-history" style={{ fontSize: "66%" }}>
                Service History
              </NavLink>
            </div>
            <div className="col-md-2">
              <NavLink
                className="navbar-brand small-link"
                to="/service-appointments"
                style={{ fontSize: "66%" }}
              >
                Service Appointments
              </NavLink>
            </div>
            <div className="col-md-2">
              <NavLink
                className="navbar-brand small-link"
                to="/create-service-appointment"
                style={{ fontSize: "66%" }}
              >
                Create a Service Appointment
              </NavLink>
            </div>
            <div className="col-md-2">
              <NavLink className="navbar-brand small-link" to="/saleshistory" style={{ fontSize: "66%" }}>
                Sale History
              </NavLink>
            </div>
          </div>
        </div>
      </div>
    </nav>
  );
}

export default Nav;
