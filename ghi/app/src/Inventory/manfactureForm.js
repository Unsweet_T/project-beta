import React, { useState } from 'react';

function ManufactureForm() {
  const [Name, setName] = useState('');


  const handleNameChange = (event) => {
    setName(event.target.value);
  };


  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {
      name:Name
    };

    const manuUrl = "http://localhost:8100/api/manufacturers/";
    const fetchOptions = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        'Content-type': 'application/json'
      },
    };

    try {
      const response = await fetch(manuUrl, fetchOptions);
      if (response.ok) {
        await response.json();
        setName('');

      }
    } catch (error) {
      console.error("Error submitting form:", error);
    }
  };

  return (
    <div className="form-row">
      <div className="col">
        <div className="shadow p-4 mt-4">
          <h1>Create a Manufacturer</h1>
          <form onSubmit={handleSubmit} id="create-salesperson-form">
            <div className="form-floating mb-3">
              <label htmlFor="first_name">Name</label>
              <input
                onChange={handleNameChange}
                placeholder="Bame"
                required
                type="text"
                name="name"
                id="name"
                className="form-control"
                value={Name}
              />
            </div>
            <div>
              <button type="submit" className="btn btn-primary">Submit</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}

export default ManufactureForm;
