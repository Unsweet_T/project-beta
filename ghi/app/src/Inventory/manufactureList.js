import React, { useState, useEffect } from 'react';

function ManufactureList(props){
    const[manufactures, setManu] = useState([]);

    const load_manu = async () => {
        const response = await fetch('http://localhost:8100/api/manufacturers/')
        if(response.ok){
            const data = await response.json();
            setManu(data.manufacturers)
        }
    }
    useEffect(() => {
        load_manu();
    }, []);


    return(
        <div>
            <h1>Manufacture List</h1>
            <table className="table table-stripped">
                <thead>
                    <tr>
                        <th>Name</th>
                    </tr>
                </thead>
                <tbody>
                    {manufactures.map(manu => {
                        return(
                            <tr key={manu.href}>
                                <td>{manu.name}</td>
                            </tr>
                        )
                    }
                        )}
                </tbody>
            </table>
        </div>
    )
}


export default ManufactureList;
