import React, { useState, useEffect } from 'react';

function ModelForm() {
  const [name, setName] = useState('');
  const [photo, setPhoto] = useState('');
  const [manu, setManu] = useState('');
  const [manus, setManus] = useState([])


  const handleNameChange = (event) => {
    setName(event.target.value);
  };
  const handlePhotoChange = (event) => {
    setPhoto(event.target.value);
  };
  const handleManuChange = (event) => {
    setManu(event.target.value);
  };


  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {
      name:name,
      picture_url:photo,
      manufacturer_id: manu
    };

    const modelUrl = "http://localhost:8100/api/models/";
    const fetchOptions = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        'content-type': 'application/json'
      },
    };

      const response = await fetch(modelUrl, fetchOptions);
      if (response.ok) {
        await response.json();
        setName('');

      }
  };
  const load_manus = async () => {
    const url = 'http://localhost:8100/api/manufacturers/';

    const response = await fetch(url);

    if (response.ok) {
    const data = await response.json();
    setManus(data.manufacturers);
    }
}
useEffect(() => {
    load_manus();
}, []);


  return (
    <div className="form-row">
      <div className="col">
        <div className="shadow p-4 mt-4">
          <h1>Create a Model</h1>
          <form onSubmit={handleSubmit} id="create-model-form">
            <div className="form-floating mb-3">
              <label htmlFor="first_name">Name</label>
              <input
                onChange={handleNameChange}
                placeholder="name"
                required
                type="text"
                name="name"
                id="name"
                className="form-control"
                value={name}
              />
            </div>
            <div className="form-floating mb-3">
              <label htmlFor="first_name">Picture Url</label>
              <input
                onChange={handlePhotoChange}
                placeholder="Bame"
                required
                type="text"
                name="name"
                id="name"
                className="form-control"
                value={photo}
              />
            </div>
            <div className="form-floating mb-3">
            <select onChange={handleManuChange} required name="manufacturer" id="manufacturer" className="form-select" value={manu}>
                                    <option value="">Choose a Manufacturer</option>
                                    {manus.map(manu =>{
                                    return(
                                        <option key={manu.href} value={manu.id}>
                                            {manu.name}
                                        </option>
                                    )
                                })}
                                </select>
            <div/>
              <button type="submit" className="btn btn-primary">Submit</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}

export default ModelForm;
