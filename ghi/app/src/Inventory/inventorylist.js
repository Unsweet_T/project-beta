import React, { useState, useEffect } from 'react';

function InventoryList(props){
    const[Inventory, setInventory] = useState([]);

    const load_Inventory = async () => {
        const response = await fetch('http://localhost:8100/api/automobiles/')
        if(response.ok){
            const data = await response.json();
            setInventory(data.autos)
        }
    }
    useEffect(() => {
        load_Inventory();
    }, []);


    return(
        <div>
            <h1>Inventory List</h1>
            <table className="table table-stripped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Color</th>
                        <th>Year</th>
                        <th>Model</th>
                        <th>Manufacturer</th>
                        <th>sold</th>
                    </tr>
                </thead>
                <tbody>
                    {Inventory.map(auto => {
                        return(
                            <tr key={auto.id}>
                                <td>{auto.vin}</td>
                                <td>{auto.color}</td>
                                <td>{auto.year}</td>
                                <td>{auto.model.name}</td>
                                <td>{auto.model.manufacturer.name}</td>
                                <td>{String(auto.sold)}</td>
                            </tr>
                        )
                    }
                        )}
                </tbody>
            </table>
        </div>
    )
}


export default InventoryList;
