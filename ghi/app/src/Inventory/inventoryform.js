import React, { useState, useEffect } from 'react';

function InventoryForm() {
  const [color, setColor] = useState('');
  const [year, setYear] = useState('');
  const [vin, setVin] = useState('');
  const [model, setModel] = useState('');
  const [models, setModels] = useState([]);

  const handleColorChange = (event) => {
    setColor(event.target.value);
  };

  const handleYearChange = (event) => {
    setYear(event.target.value);
  };

  const handleVinChange = (event) => {
    setVin(event.target.value);
  };

  const handleModelChange = (event) => {
    setModel(event.target.value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {
      color: color,
      year: year,
      vin: vin,
      model_id: model,
    };
    console.log(data)
    const autoUrl = "http://localhost:8100/api/automobiles/";
    const fetchOptions = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        'Content-type': 'application/json'
      },
    };


      const response = await fetch(autoUrl, fetchOptions);
      if (response.ok) {
        await response.json();
        setColor('');
        setYear('');
        setVin('');
        setModel('');
      }
  };

  const loadModels = async () => {
    const url = 'http://localhost:8100/api/models/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setModels(data.models);
    }
  }

  useEffect(() => {
    loadModels();
  }, []);

  return (
    <div className="form-row">
      <div className="col">
        <div className="shadow p-4 mt-4">
          <h1>Add an automobile to inventory</h1>
          <form onSubmit={handleSubmit} id="create-automobile-form">
            <div className="form-floating mb-3">
              <label htmlFor="color">Color</label>
              <input
                onChange={handleColorChange}
                placeholder="Color"
                required
                type="text"
                name="color"
                id="color"
                className="form-control"
                value={color}
              />
            </div>
            <div className="form-floating mb-3">
              <label htmlFor="Year">Year</label>
              <input
                onChange={handleYearChange}
                placeholder="Year"
                required
                type="text"
                name="year"
                id="year"
                className="form-control"
                value={year}
              />
            </div>
            <div className="form-floating mb-3">
              <label htmlFor="VIN">VIN</label>
              <input
                onChange={handleVinChange}
                placeholder="VIN"
                required
                type="text"
                name="VIN"
                id="VIN"
                className="form-control"
                value={vin}
              />
            </div>
            <div className="form-floating mb-3">
              <label htmlFor="models"></label>
              <select
                onChange={handleModelChange}
                required
                name="models"
                id="models"
                className="form-select"
                value={model}
              >
                <option value="">Choose a Model</option>
                {models.map(model => (
                  <option key={model.id} value={model.id}>
                    {model.name}
                  </option>
                ))}
              </select>
            </div>
            <div>
              <button type="submit" className="btn btn-primary">Submit</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}

export default InventoryForm;
