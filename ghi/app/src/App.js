import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import SalesPersonForm from './Sales/AddSalesPerson';
import SalesPersonList from './Sales/SalesPersonList';
import CustomerList from './Sales/Customerlist';
import CustomerForm from './Sales/customerform';
import SaleList from './Sales/salelist';
import SaleForm from './Sales/SaleForm';
import ManufactureList from './Inventory/manufactureList';
import ManufactureForm from './Inventory/manfactureForm';
import ModelList from './Inventory/modelList';
import ModelForm from './Inventory/modelForm';
import InventoryList from './Inventory/inventorylist';
import InventoryForm from './Inventory/inventoryform';
import ListServiceAppointments from "./Service/ListServiceAppointments";
import AppointmentForm from "./Service/AppointmentForm";
import ServiceHistory from "./Service/ServiceHistory";
import ListAllTechnicians from "./Service/ListAllTechnicians";
import AddTechnicianForm from "./Service/AddTechnicianForm";
import SalesHistory from './Sales/saleshistory';


function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/automobiles" element={<InventoryList autos={props.autos} />} />
          <Route path="/create-automobile" element={<InventoryForm />} />
          <Route path="/manufacturers" element={<ManufactureList manufactures={props.manufactures} />} />
          <Route path="/new-manufacturer" element={<ManufactureForm />} />
          <Route path="models/" element={<ModelList models={props.models} />} />
          <Route path="/create-model" element={<ModelForm />} />
          <Route path="/add-sales-person" element={<SalesPersonForm />} />
          <Route path="/sales-team" element={<SalesPersonList salesteam={props.salesteam} />} />
          <Route path="/customer-list" element={<CustomerList Customers={props.Customers} />} />
          <Route path="/new-customer" element={<CustomerForm />} />
          <Route path="/sales-list" element={<SaleList sales={props.sales} />} />
          <Route path="/make-sale" element={<SaleForm />} />
          <Route path="/saleshistory" element={<SalesHistory />} />
          <Route path="/service-appointments" element={<ListServiceAppointments {...props} />} />
          <Route path="/create-service-appointment" element={<AppointmentForm />} />
          <Route path="/service-history" element={<ServiceHistory {...props}/>} />
          <Route path="/technicians" element={<ListAllTechnicians {...props}/>} />
          <Route path="/add-technician" element={<AddTechnicianForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
