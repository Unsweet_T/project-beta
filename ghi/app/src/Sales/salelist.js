import React, { useState, useEffect } from 'react';

function SaleList(){
    const[sales, setSale] = useState([]);

    const load_sales = async () => {
        const response = await fetch('http://localhost:8090/api/sales/')
        if(response.ok){
            const data = await response.json();
            setSale(data.sales)
        }
    }
    useEffect(() => {
        load_sales();
    }, []);


    return(
        <div>
            <h1>Sale List</h1>
            <table className="table table-stripped">
                <thead>
                    <tr>
                        <th>Salesperson ID</th>
                        <th>Salesperson Name</th>
                        <th>Customer Name</th>
                        <th>VIN</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {sales.map(sale => {
                        return(
                            <tr key={sale.id}>
                                <td>{sale.salesperson.employee_id}</td>
                                <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                                <td>{sale.customer.first_name} {sale.customer.last_name}</td>
                                <td>{sale.automobile}</td>
                                <td>{sale.price}</td>
                            </tr>
                        )
                    }
                        )}
                </tbody>
            </table>
        </div>
    )
}


export default SaleList;
