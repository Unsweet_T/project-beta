import React, { useState, useEffect } from 'react';

function SalesPersonList(props) {
  const [salesrep, setSalesrep] = useState([]);

  const load_salesperson = async () => {
    const response = await fetch('http://localhost:8090/api/salespeople/');
    if (response.ok) {
      const data = await response.json();
      setSalesrep(data.salesteam);
    }
  };

  useEffect(() => {
    load_salesperson();
  }, []);

  return (
    <div>
      <h1>Sales Team</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Employee ID</th>
            <th>First Name</th>
            <th>Last Name</th>
          </tr>
        </thead>
        <tbody>
          {salesrep.map((sale) => (
            <tr key={sale.id}>
              <td>{sale.employee_id}</td>
              <td>{sale.first_name}</td>
              <td>{sale.last_name}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default SalesPersonList;
