import React, { useEffect, useState } from "react";

function SalesHistory() {
  const [salespeople, setSales] = useState([]);
  const [salespersonhis, setHistory] = useState([]);
  const [salesperson, setSalesrep] = useState("");

  const handleSalesChange = (event) => {
    const value = event.target.value;
    setSalesrep(value);
  };

  const load_salesrep = async () => {
    const response = await fetch("http://localhost:8090/api/salespeople/");
    if (response.ok) {
      const data = await response.json();
      setSales(data.salesteam);
    }
  };

  const load_sales = async () => {
    const response = await fetch("http://localhost:8090/api/sales/");
    if (response.ok) {
      const data = await response.json();
      setHistory(data.sales);
    }
  };

  useEffect(() => {
    load_salesrep();
    load_sales();
  }, []);

  return (
    <div>
      <h1>Customer List</h1>
      <select
        onChange={handleSalesChange}
        value={salesperson}
        required
        name="salesperson"
        id="salesperson"
      >
        <option value="">Select a Salesperson</option>
        {salespeople.map((salesperson) => (
          <option key={salesperson.id} value={salesperson.employee_id}>
            {salesperson.employee_id}
          </option>
        ))}
      </select>

      <table className="table table-stripped">
        <thead>
          <tr>
            <th>Salesperson</th>
            <th>Customer</th>
            <th>VIN</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>
          {salespersonhis
            .filter((sale) => sale.salesperson.employee_id === salesperson)
            .map((sale) => {
              return (
                <tr key={sale.id}>
                  <td>{sale.salesperson.employee_id}</td>
                  <td>
                    {sale.customer.first_name} {sale.customer.last_name}
                  </td>
                  <td>{sale.automobile}</td>
                  <td>${sale.price}</td>
                </tr>
              );
            })}
        </tbody>
      </table>
    </div>
  );
}

export default SalesHistory;
