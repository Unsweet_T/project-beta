import React, { useState } from 'react';

function CustomerForm() {
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [address, setAddress] = useState('');
  const [phone, setNumber] = useState('');

  const handleFirstNameChange = (event) => {
    setFirstName(event.target.value);
  };

  const handleLastNameChange = (event) => {
    setLastName(event.target.value);
  };

  const handleaddressChange = (event) => {
    setAddress(event.target.value);
  };
  const handlenumberchange = (event) => {
    setNumber(event.target.value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {
      first_name: firstName,
      last_name: lastName,
      address: address,
      phone_number: phone,
    };

    const custUrl = "http://localhost:8090/api/customers/";
    const fetchOptions = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        'Content-type': 'application/json'
      },
    };

    try {
      const response = await fetch(custUrl, fetchOptions);
      if (response.ok) {
        await response.json();
        setFirstName('');
        setLastName('');
        setAddress('');
        setNumber('');
      }
    } catch (error) {
      console.error("Error submitting form:", error);
    }
  };

  return (
    <div className="form-row">
      <div className="col">
        <div className="shadow p-4 mt-4">
          <h1>Create a Customer</h1>
          <form onSubmit={handleSubmit} id="create-salesperson-form">
            <div className="form-floating mb-3">
              <label htmlFor="first_name">First Name</label>
              <input
                onChange={handleFirstNameChange}
                placeholder="First Name"
                required
                type="text"
                name="first_name"
                id="first_name"
                className="form-control"
                value={firstName}
              />
            </div>
            <div className="form-floating mb-3">
              <label htmlFor="last_name">Last Name</label>
              <input
                onChange={handleLastNameChange}
                placeholder="Last Name"
                required
                type="text"
                name="last_name"
                id="last_name"
                className="form-control"
                value={lastName}
              />
            </div>
            <div className="form-floating mb-3">
              <label htmlFor="Address">Address</label>
              <input
                onChange={handleaddressChange}
                placeholder="Address"
                required
                type="text"
                name="Address"
                id="Address"
                className="form-control"
                value={address}
              />
            </div>
            <div className="form-floating mb-3">
              <label htmlFor="Address">Phone Number</label>
              <input
                onChange={handlenumberchange}
                placeholder="Number"
                required
                type="text"
                name="Number"
                id="Number"
                className="form-control"
                value={phone}
              />
            </div>
            <div>
              <button type="submit" className="btn btn-primary">Submit</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}

export default CustomerForm;
