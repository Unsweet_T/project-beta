import React, { useEffect, useState } from "react";

function SaleForm(props) {
  const [vin, setVin] = useState("");
  const [salesperson, setPerson] = useState("");
  const [customer, setCustomer] = useState("");
  const [price, setPrice] = useState("");
  const [vins, setVins] = useState([]);
  const [salespeople, setPeople] = useState([]);
  const [customers, setCustomers] = useState([]);
  const [vinSold, setVinSold] = useState(false);

  const handleVinChange = (event) => {
    setVin(event.target.value);
  };
  const handleSalesPersonChange = (event) => {
    setPerson(event.target.value);
  };
  const handleCustomerChange = (event) => {
    setCustomer(event.target.value);
  };
  const handlePriceChange = (event) => {
    setPrice(event.target.value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {
      automobile: vin,
      salesperson: salesperson,
      customer: customer,
      price: price,
    };

    const saleURl = "http://localhost:8090/api/sales/";
    const fetchOptions = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-type": "application/json",
      },
    };

    const response = await fetch(saleURl, fetchOptions);
    if (response.ok) {
      await response.json();


      const autoUrl = `http://localhost:8100/api/automobiles/${vin}/`;
      const autoConfig = {
        method: "PUT",
        body: JSON.stringify({ sold: true }),
        headers: {
          "Content-type": "application/json",
        },
      };

      const autoResponse = await fetch(autoUrl, autoConfig);
      if (autoResponse.ok) {
        setVin("");
        setPerson("");
        setCustomer("");
        setPrice("");
      }
    }
  };

  const loadVins = async () => {
    const vinurl = "http://localhost:8100/api/automobiles/";
    const vinresponse = await fetch(vinurl);

    if (vinresponse.ok) {
      const data = await vinresponse.json();
      setVins(data.autos);
    }
  };

  const loadSalesTeam = async () => {
    const teamurl = "http://localhost:8090/api/salespeople/";
    const teamresponse = await fetch(teamurl);

    if (teamresponse.ok) {
      const data = await teamresponse.json();
      setPeople(data.salesteam);
    }
  };

  const loadCustomers = async () => {
    const custurl = "http://localhost:8090/api/customers/";
    const custresponse = await fetch(custurl);

    if (custresponse.ok) {
      const data = await custresponse.json();
      setCustomers(data.Customers);
    }
  };

  const checkVinSold = async (vin) => {
    const autoUrl = `http://localhost:8100/api/automobiles/${vin}/`;
    const autoResponse = await fetch(autoUrl);

    if (autoResponse.ok) {
      const data = await autoResponse.json();
      setVinSold(data.sold);
    }
  };

  useEffect(() => {
    loadVins();
    loadCustomers();
    loadSalesTeam();
  }, []);

  useEffect(() => {
    if (vin) {
      checkVinSold(vin);
    }
  }, [vin]);

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Record a new sale</h1>
          <form onSubmit={handleSubmit} id="create-sale-form">
            <div className="mb-3">
              <select
                value={vin}
                onChange={handleVinChange}
                required
                id="automobile"
                name="automobile"
                className="form-select"
              >
                <option value="">Choose an automobile VIN</option>
                {vins.map((vin) => {
                  return (
                    <option key={vin.href} value={vin.href.id}>
                      {vin.vin}
                    </option>
                  );
                })}
              </select>
              {vinSold && (
                <p className="text-danger">This VIN has already been sold. Pick Another VIN please.</p>
              )}
            </div>
            <div className="mb-3">
              <select
                value={salesperson}
                onChange={handleSalesPersonChange}
                required
                id="salesperson"
                name="salesperson"
                className="form-select"
              >
                <option value="">Choose a salesperson</option>
                {salespeople.map((salesperson) => {
                  return (
                    <option key={salesperson.id} value={salesperson.id}>
                      {salesperson.first_name} {salesperson.last_name}
                    </option>
                  );
                })}
              </select>
            </div>
            <div className="mb-3">
              <select
                value={customer}
                onChange={handleCustomerChange}
                required
                id="customer"
                name="customer"
                className="form-select"
              >
                <option value="">Choose a customer</option>
                {customers.map((customer) => {
                  return (
                    <option key={customer.id} value={customer.id}>
                      {customer.first_name} {customer.last_name}
                    </option>
                  );
                })}
              </select>
            </div>
            <div className="form-floating mb-3">
              <input
                value={price}
                onChange={handlePriceChange}
                placeholder="Price"
                required
                type="number"
                name="price"
                id="price"
                className="form-control"
              />
              <label htmlFor="price">Price</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default SaleForm;
