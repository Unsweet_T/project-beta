import React, { useState } from 'react';

function SalesPersonForm() {
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [employeeID, setEmployeeID] = useState('');

  const handleFirstNameChange = (event) => {
    setFirstName(event.target.value);
  };

  const handleLastNameChange = (event) => {
    setLastName(event.target.value);
  };

  const handleEmployeeIDChange = (event) => {
    setEmployeeID(event.target.value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {
      first_name: firstName,
      last_name: lastName,
      employee_id: employeeID,
    };

    console.log(data);

    const salespersonUrl = "http://localhost:8090/api/salespeople/";
    const fetchOptions = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        'Content-type': 'application/json'
      },
    };

    try {
      const response = await fetch(salespersonUrl, fetchOptions);
      if (response.ok) {
        await response.json();
        setFirstName('');
        setLastName('');
        setEmployeeID('');
      }
    } catch (error) {
      console.error("Error submitting form:", error);
    }
  };

  return (
    <div className="form-row">
      <div className="col">
        <div className="shadow p-4 mt-4">
          <h1>Create a Sales Person</h1>
          <form onSubmit={handleSubmit} id="create-salesperson-form">
            <div className="form-floating mb-3">
              <label htmlFor="first_name">First Name</label>
              <input
                onChange={handleFirstNameChange}
                placeholder="First Name"
                required
                type="text"
                name="first_name"
                id="first_name"
                className="form-control"
                value={firstName}
              />
            </div>
            <div className="form-floating mb-3">
              <label htmlFor="last_name">Last Name</label>
              <input onChange={handleLastNameChange}
                placeholder="Last Name"
                required
                type="text"
                name="last_name"
                id="last_name"
                className="form-control"
                value={lastName}
              />
            </div>
            <div className="form-floating mb-3">
              <label htmlFor="employee_id">Employee ID</label>
              <input
                onChange={handleEmployeeIDChange}
                placeholder="Employee ID"
                required
                type="text"
                name="employee_id"
                id="employee_id"
                className="form-control"
                value={employeeID}
              />
            </div>
            <div>
              <button type="submit" className="btn btn-primary">Submit</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}

export default SalesPersonForm;
