import React, { useState, useEffect } from "react";

function AppointmentForm() {
  const [vin, setVin] = useState("");
  const [customer, setCustomer] = useState("");
  const [reason, setReason] = useState("");
  const [date, setDate] = useState("");
  const [time, setTime] = useState("");
  const [technicians, setTechnicians] = useState([]);
  const [selectedTechnician, setSelectedTechnician] = useState("");

  useEffect(() => {
    getTechnicians();
  }, []);

  async function getTechnicians() {
    const response = await fetch("http://localhost:8080/api/technicians/");
    const data = await response.json();
    setTechnicians(data.technicians);
  }

  const handleSubmit = async (event) => {
    event.preventDefault();
    const apptsUrl = "http://localhost:8080/api/appointments/";
    const dateTime = new Date(`${date} ${time}`);

    const selectedTechnicianObj = technicians.find(
      (tech) => tech.employee_id === selectedTechnician
    );

    const fetchOptions = {
      method: "POST",
      body: JSON.stringify({
        vin: vin,
        customer: customer,
        customerreason: reason,
        date_time: dateTime,
        technician: { employee_id: selectedTechnicianObj.employee_id },
      }),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(apptsUrl, fetchOptions);

    if (response.ok) {
      await response.json();
      setVin("");
      setCustomer("");
      setReason("");
      setDate("");
      setTime("");
      setSelectedTechnician("");
    }
  };

  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        marginTop: "20px",
      }}
    >
      <h2>Create a Service Appointment</h2>
      <form onSubmit={handleSubmit}>
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            marginBottom: "20px",
          }}
        >
          <label htmlFor="vin">VIN:</label>
          <input
            type="text"
            id="vin"
            value={vin}
            onChange={(event) => setVin(event.target.value)}
            required
          />
        </div>
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            marginBottom: "20px",
          }}
        >
          <label htmlFor="customer">Customer:</label>
          <input
            type="text"
            id="customer"
            value={customer}
            onChange={(event) => setCustomer(event.target.value)}
            required
          />
        </div>
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            marginBottom: "20px",
          }}
        >
          <label htmlFor="reason">Reason:</label>
          <input
            type="text"
            id="reason"
            value={reason}
            onChange={(event) => setReason(event.target.value)}
            required
          />
        </div>
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            marginBottom: "20px",
          }}
        >
          <label htmlFor="date">Date:</label>
          <input
            type="date"
            id="date"
            value={date}
            onChange={(event) => setDate(event.target.value)}
            required
          />
        </div>
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            marginBottom: "20px",
          }}
        >
          <label htmlFor="time">Time:</label>
          <input
            type="time"
            id="time"
            value={time}
            onChange={(event) => setTime(event.target.value)}
            required
          />
        </div>
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            marginBottom: "20px",
          }}
        >
          <label htmlFor="technician">Your Technician:</label>
          <select
            id="technician"
            value={selectedTechnician}
            onChange={(event) => setSelectedTechnician(event.target.value)}
            required
          >
            <option value="">Select a technician</option>
            {technicians.map((technician) => (
              <option
                key={technician.employee_id}
                value={technician.employee_id}
              >
                {technician.first_name} {technician.last_name}
              </option>
            ))}
          </select>
        </div>
        <div style={{ display: "flex", justifyContent: "center" }}>
          <button type="submit">Create Appointment</button>
        </div>
      </form>
    </div>
  );
}

export default AppointmentForm;
