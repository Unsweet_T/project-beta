import React, { useEffect, useState } from "react";

function ServiceHistory() {
    const [appointments, setAppointments] = useState([]);
    const [searchQuery, setSearchQuery] = useState("");

    useEffect(() => {
        fetchAppointments();
    }, []);

    const fetchAppointments = async () => {
        try {
            const response = await fetch("http://localhost:8080/api/appointments/");
            const data = await response.json();
            setAppointments(data);
        } catch (error) {
            console.error("Error fetching service history:", error);
        }
    };

    const handleSearch = (event) => {
        setSearchQuery(event.target.value.toUpperCase());
    };

    const filteredAppointments = appointments.filter((appointment) =>
        appointment.vin.includes(searchQuery)
    );

    return (
        <div>
            <h2>Service History</h2>
            <div>
                <label htmlFor="search">Search by VIN:</label>
                <input type="text" id="search" value={searchQuery} onChange={handleSearch} />
            </div>
            <table className="table table-stripped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Customer</th>
                        <th>Date/Time</th>
                        <th>Is VIP?</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    {filteredAppointments.map((appointment) => (
                        <tr key={appointment.id}>
                            <td style={{ fontSize: "80%" }}>{appointment.vin}</td>
                            <td style={{ fontSize: "80%" }}>{appointment.customer}</td>
                            <td style={{ fontSize: "80%" }}>{new Date(appointment.date_time).toLocaleDateString("en-US", {
                                year: "2-digit",
                                month: "short",
                                day: "2-digit",
                            })}</td>
                            <td style={{ fontSize: "80%" }}>{appointment.vip ? "Yes" : "No"}</td>
                            <td style={{ fontSize: "80%" }}>{appointment.technician}</td>
                            <td style={{ fontSize: "80%" }}>{appointment.customerreason}</td>
                            <td style={{ fontSize: "80%" }}>{appointment.status}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
}

export default ServiceHistory;
