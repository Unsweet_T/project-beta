import React, { useState } from "react";

function AddTechnicianForm() {
    const [employeeId, setEmployeeId] = useState("");
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");

    const handleSubmit = async (event) => {
        event.preventDefault();

        const url = "http://localhost:8080/api/technicians/";

        const technicianData = {
            employee_id: employeeId,
            first_name: firstName,
            last_name: lastName,
        };

        try {
            const response = await fetch(url, {
                method: "POST",
                headers: {
                "Content-Type": "application/json",
                },
                body: JSON.stringify(technicianData),
            });

            if (response.ok) {
                console.log("Technician created successfully");
                
                setEmployeeId("");
                setFirstName("");
                setLastName("");
            } else {
                console.error("Failed to create technician:", response.status, response.statusText);
            }
        } catch (error) {
            console.error("Error:", error);
        }
    };

    return (
        <div style={{ display: "flex", flexDirection: "column", alignItems: "center", marginTop: "20px" }}>
            <h2>Add Technician</h2>
            <form onSubmit={handleSubmit}>
                <div style={{ display: "flex", justifyContent: "space-between", marginBottom: "20px" }}>
                    <label htmlFor="employeeId">Employee ID:</label>
                    <input type="text" id="employeeId" value={employeeId} onChange={(event) => setEmployeeId(event.target.value)} required />
                </div>
                <div style={{ display: "flex", justifyContent: "space-between", marginBottom: "20px" }}>
                    <label htmlFor="firstName">First Name:</label>
                    <input type="text" id="firstName" value={firstName} onChange={(event) => setFirstName(event.target.value)} required />
                </div>
                <div style={{ display: "flex", justifyContent: "space-between", marginBottom: "20px" }}>
                    <label htmlFor="lastName">Last Name:</label>
                    <input type="text" id="lastName" value={lastName} onChange={(event) => setLastName(event.target.value)} required />
                </div>
                <div style={{ display: "flex", justifyContent: "center" }}>
                    <button type="submit">Add Technician</button>
                </div>
            </form>
        </div>
    );
}

export default AddTechnicianForm;