import React, { useEffect, useState } from "react";

function ListServiceAppointments() {
    const [appointments, setAppointments] = useState([]);

    useEffect(() => {
        fetchAppointments();
    }, []);

    const fetchAppointments = async () => {
        try {
            const response = await fetch("http://localhost:8080/api/appointments");
            if (response.ok) {
                const data = await response.json();
                setAppointments(data);
            } else {
                console.error("Failed to fetch appointments:", response.status, response.statusText);
            }
        } catch (error) {
            console.error("Error fetching appointments:", error);
        }
    };

    const handleCancelAppointment = async (event, id) => {
        event.preventDefault();
        try {
            const response = await fetch(`http://localhost:8080/api/appointments/${id}/cancel/`, {
                method: "PUT",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify({
                    status: "Cancel",
                }),
            });
            if (response.ok) {
                console.log("Appointment canceled successfully");
                fetchAppointments();
            } else {
                console.error("Failed to cancel the appointment:", response.status, response.statusText);
            }
        } catch (error) {
            console.error("Error canceling appointment:", error);
        }
    };
    
    const handleFinishAppointment = async (event, id) => {
        event.preventDefault();
        try {
            const response = await fetch(`http://localhost:8080/api/appointments/${id}/finish/`, {
                method: "PUT",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify({
                    status: "Finish",
                }),
            });
            if (response.ok) {
                console.log("Appointment finished successfully");
                fetchAppointments();
            } else {
                console.error("Failed to finish the appointment:", response.status, response.statusText);
            }
        } catch (error) {
            console.error("Error finishing appointment:", error);
        }
    };

    return (
        <div>
            <h2>List of Service Appointments</h2>
            <table className="table table-stripped">
                <thead>
                    <tr>
                        <th>Date and Time</th>
                        <th>VIN</th>
                        <th>Customer</th>
                        <th>Reason</th>
                        <th>Technician</th>
                        <th>Status</th>
                        <th>Change Status</th>
                    </tr>
                </thead>
                <tbody>
                    {appointments.map((appointment) => (
                        <tr key={appointment.id}>
                            <td>{appointment.date_time}</td>
                            <td>{appointment.vin}</td>
                            <td>{appointment.customer}</td>
                            <td>{appointment.customerreason}</td>
                            <td>{appointment.technician}</td>
                            <td>{appointment.status}</td>
                            <td>
                                {appointment.status !== "Canceled" && appointment.status !== "Finished" && (
                                    <>
                                        <button onClick={(event) => handleCancelAppointment(event, appointment.id)}>Cancel</button>
                                        <button onClick={(event) => handleFinishAppointment(event, appointment.id)}>Finish</button>
                                    </>
                                )}
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
}

export default ListServiceAppointments;