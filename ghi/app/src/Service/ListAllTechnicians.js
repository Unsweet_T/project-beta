import React, { useEffect, useState } from "react";

function ListAllTechnicians() {
    const [technicians, setTechnicians] = useState([]);

    useEffect(() => {
        fetchTechnicians();
    }, []);

    const fetchTechnicians = async () => {

        try {
            const response = await fetch("http://localhost:8080/api/technicians/");
            const data = await response.json();
            setTechnicians(data.technicians);
        } catch (error) {
            console.error("Error fetching technicians:", error);
        }
    };

    return (
        <div>
            <h2>List of Technicians</h2>
            <table>
                <thead>
                    <tr>
                        <th>Employee ID</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                    </tr>
                </thead>
                <tbody>
                    {technicians.map((technician) => (
                        <tr key={technician.id}>
                            <td>{technician.employee_id}</td>
                            <td>{technician.first_name}</td>
                            <td>{technician.last_name}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
}

export default ListAllTechnicians;