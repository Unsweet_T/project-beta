# CarCar

Team:

* Timothy - Which microservice? Sales
* Person 2 - Which microservice?
Thomas - Service

## Design
CarCar was designed as a microservice-based system that uses separate microserviceses to handle the sales and service operations.  The microservices communicate through APIs and the system is designed with a frontend React client that uses those RESTful APIs to talk with the backend microservices.  The backend is a Django framework and with it we developed the microservices that were containerized and deployed using docker.

## Service microservice

My Technician model was created to represent the person who performs the service on customer vehicles.  Within the model I define properties for the first_name, last_name, and a unique, but user-defined employee_id.  This model is utilized with the Appointment model to assign a technician to a service appointment.

My AutomobileVO model is a value object for the Automobile model.  This model is used to verify whether or not a vin from a service appointment vehicle matches a vin from a vehicle that was previously sold by the dealership. If the a vehicle from the inventory contains a boolean of "True" for the sold property and the vin matches the vin from a service appointment, the vip property of the Appointment will be set to "True"  I used the backend to automate this process upon the creation of an appointment.  I did not have time to fully test.

My Appointment model represents the minimum properties needed to perform service functions for this system.  The date and time of an appointment are scheduled using the date_time property.  The user inputs a customer's name, reason, and vin.  Technician_notes are not currently used and will be an added feature.  The status is set to "Active" upon creating an appointment and changes to "canceled" or "finished" by the user. The vip property is not user defined and uses the AutomobileVO to communicate using RESTful APis to the inventory microservice for that data.  The technician for an appointment is assigned through a dropdown list of current technicians.

In short, microservice integration takes place by using a poller to fetch the vin and sold data from the inventory.  The fetched data is then used to populate my AutomobileVO model within the service microservice allowing me to use the information to determine whether or not my service customer is a vip. 

## Sales microservice

Explain your models and integration with the inventory
microservice, here.

When creating CarCar the models I implemented 4 different models one model for the Salesperson which takes in "First_name", "last_name" and "employee_id". This model was implemented to manage the sales team. With this model I was able to add create and delete a sales person. Next I created the customer model which keeps track of the customers that came to the dealership I took in the same fields as the sales team but leaving out "employee_id" and taking in their address and their numbers. Next we have the Sale model which is a model that has three foregin keys which are automobile, salesperson, and customer. We have it this way because the same customer can purchase multiple vehicles while the salesperson can sell mutiple vehicles. And lastly for the automobiles we use a foregin key because their is a inventory of automobiles that will be sold not just one. Finally we have out AutomobileVO which allows sales to communicate with the inventory microservice. In the AutomobileVO theres only two fields one being the VIN and  the other being a sold field. these two fields allows the sales microservice and the service microservice to communicate with eachother.

They communicate by VIN and sold, in the sales microservice every automobile that comes into the dealership comes in with a VIN number and the sold field set on false as default because any vehicle that is in the sales inventory needs to be sold. With that being said once I create a sale CarCar sends a POST method to the sales api to keep track of what car was sold via VIN. Addtionally it shows who sold the car from the sales team and who bought the car from the customer model and for what price. After sending the POST method to the sales api the inventory microservice needs to be updated. CarCar goes into the inventory api and retrieves the car that has been sold via VIN and changes their sold field to true.
