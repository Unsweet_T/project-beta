# Generated by Django 4.0.3 on 2023-06-06 19:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sales_rest', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='automobilevo',
            name='import_href',
        ),
        migrations.AlterField(
            model_name='salesperson',
            name='employee_id',
            field=models.CharField(default=False, max_length=200),
        ),
    ]
