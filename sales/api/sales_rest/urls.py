from django.urls import path
from .views import (
    api_salesperson_list,
    api_show_salesperson,
    api_customer_list,
    api_show_customer,
    api_sales_list,
    api_show_sale,
    api_list_automobileVO)


urlpatterns = [
    path("salespeople/" ,api_salesperson_list, name="api_salesperson_list"),
    path("salespeople/<int:id>/", api_show_salesperson, name="api_show_salesperson"),
    path("customers/", api_customer_list, name="api_customer_list"),
    path("customers/<int:id>/", api_show_customer, name="api_show_customer"),
    path("sales/", api_sales_list, name="api_sales_list"),
    path("sales/<int:id>/", api_show_sale, name="api_show_sale"),
    path("automobiles/", api_list_automobileVO, name="api_list_automobiles" ),
]
