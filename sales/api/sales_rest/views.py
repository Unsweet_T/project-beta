from django.shortcuts import render
from sales_rest.models import Salesperson, AutomobileVO ,Customer, Sale
from django.http import JsonResponse
import json
from django.views.decorators.http import require_http_methods
from .encoders import SaleEncoder,SalesPersonDetailEncoder,CustomerEncoder,AutomobileVOEncoder

# Create your views here.

@require_http_methods(["GET", "POST"])
def api_salesperson_list(request):
    if request.method == "GET":
        sales = Salesperson.objects.all()
        return JsonResponse(
            {"salesteam": sales},
            encoder=SalesPersonDetailEncoder
        )

    else:
        try:
            content = json.loads(request.body)
            salesperson = Salesperson.objects.create(**content)
            return JsonResponse(
                salesperson,
                encoder=SalesPersonDetailEncoder,
                safe=False
            )
        except:
            response = JsonResponse(
                {"Message": "invalid sales member"}
            )

            response.status_code = 400
            return response



@require_http_methods(["DELETE","GET"])
def api_show_salesperson(request, id):
    emp = Salesperson.objects.get(id=id)
    if request == "GET":
            return JsonResponse(
                {"sales": emp},encoder=SalesPersonDetailEncoder,
            )
    else:
        count, _ = Salesperson.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})


@require_http_methods(["GET","POST"])
def api_customer_list(request):
    if request.method == "GET":
        try:
            customer = Customer.objects.all()
            return JsonResponse(
                {"Customers": customer},
                encoder=CustomerEncoder,
            )
        except Customer.DoesNotExist:
            return JsonResponse({"message": "Invalid"}, status=400)
    else:
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
        )



@require_http_methods(["DELETE","GET"])
def api_show_customer(request, id):
    cust = Customer.objects.get(id=id)
    if request == "GET":
        return JsonResponse(
            {"Customers": cust},encoder=CustomerEncoder,
        )
    else:
        count, _ = Customer.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})


@require_http_methods(["GET", "POST"])
def api_sales_list(request, automobile_vo_id=None):
    if request.method == "GET":
        if automobile_vo_id is not None:
            sales = Sale.objects.filter(automobile=automobile_vo_id)
        else:
            sales = Sale.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SaleEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            automobile_vin = content["automobile"]
            automobile = AutomobileVO.objects.get(vin=automobile_vin)
            content["automobile"] = automobile
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid automobile VIN"},
                status=400,
            )
        try:
            salesperson = Salesperson.objects.get(id=content["salesperson"])
            content["salesperson"] = salesperson
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid salesperson id"},
                status=400,
            )
        try:
            customer = Customer.objects.get(id=content["customer"])
            content["customer"] = customer
        except Customer.DoesNotExist:
            return JsonResponse({"message": "Invalid customer id"}, status=400)
        sales = Sale.objects.create(**content)
        return JsonResponse(sales, encoder=SaleEncoder, safe=False)




@require_http_methods(["DELETE","GET"])
def api_show_sale(request, id):
    sale = Sale.objects.get(id=id)
    if request == "GET":
        return JsonResponse(
            {"Sales": sale},encoder=SaleEncoder,
        )
    else:
        count, _ = Sale.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})

@require_http_methods(["GET"])
def api_list_automobileVO(request):
    if request.method == "GET":
        automobiles = AutomobileVO.objects.all()
        return JsonResponse({
            "automobiles": automobiles},
            encoder = AutomobileVOEncoder
        )
